// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// #define CDSO(ExistingComponent, Type) ExistingComponent = CreateDefaultSubobject<Type>(*ToSentenceCase(TEXT(#ExistingComponent)));

#define CDSO(ExistingComponent, Type) ExistingComponent = CreateDefaultSubobject<Type>(TEXT(#ExistingComponent));

// FString ToSentenceCase(const FString& Str);