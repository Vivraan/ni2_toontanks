// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "ProjectileBase.generated.h"

UCLASS()
class TOONTANKS_API AProjectileBase : public AActor
{
    GENERATED_BODY()

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UProjectileMovementComponent* ProjectileMovement;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* ProjectileMesh;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UParticleSystemComponent* ProjectileTrail;


    UPROPERTY(EditDefaultsOnly, Category = "Properties|Damage")
    TSubclassOf<UDamageType> DamageType;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties|Movement", meta = (AllowPrivateAccess = "true"))
    float MovementSpeed = 1500.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties|Damage", meta = (AllowPrivateAccess = "true"))
    float Damage = 50.f;

    UPROPERTY(EditAnywhere, Category = "Properties|Effects")
    UParticleSystem* HitParticles;

    UPROPERTY(EditAnywhere, Category = "Properties|Effects")
    USoundBase* HitSound;

    UPROPERTY(EditAnywhere, Category = "Properties|Effects")
    USoundBase* LaunchSound;

    UPROPERTY(EditAnywhere, Category = "Properties|Effects")
    TSubclassOf<UCameraShakeBase> HitShake;


    UFUNCTION()
    void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

public:
    // Sets default values for this actor's properties
    AProjectileBase();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
};
