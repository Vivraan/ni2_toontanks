// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "ToonTankGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class TOONTANKS_API AToonTankGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

private:
    class APawnTank* PlayerTank;
    int32 NumTargetTowers = 0;
    class APlayerControllerBase* PlayerController;

    void OnGameStart();

    void OnGameOver(bool bPlayerWon);

public:
    void ActorDied(AActor* DeadActor);

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game Loop")
    int32 StartDelay = 3;

    virtual void BeginPlay() override;

    UFUNCTION(BlueprintImplementableEvent)
    void GameStart();

    UFUNCTION(BlueprintImplementableEvent)
    void GameOver(bool bPlayerWon);
};
