// Fill out your copyright notice in the Description page of Project Settings.


#include "ToonTankGameModeBase.h"

#include "Kismet/GameplayStatics.h"
#include "ToonTanks/Controllers/PlayerControllerBase.h"
#include "ToonTanks/Pawns/PawnTank.h"
#include "ToonTanks/Pawns/PawnTower.h"

void AToonTankGameModeBase::BeginPlay()
{
    Super::BeginPlay();

    TArray<AActor*> Towers;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTower::StaticClass(), Towers);
    NumTargetTowers = Towers.Num();

    PlayerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));

    OnGameStart();
}

void AToonTankGameModeBase::ActorDied(AActor* DeadActor)
{
    if (DeadActor == PlayerTank)
    {
        PlayerTank->OnDeath();
        OnGameOver(false);

        if (PlayerController)
        {
            PlayerController->SetPlayerEnabledState(false);
        }
    }
    else if (APawnTower* DestroyedTower = Cast<APawnTower>(DeadActor))
    {
        DestroyedTower->OnDeath();

        if (--NumTargetTowers <= 0)
        {
            OnGameOver(true);
        }
    }
}

void AToonTankGameModeBase::OnGameStart()
{
    PlayerController = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));

    GameStart();

    if (PlayerController)
    {
        PlayerController->SetPlayerEnabledState(false);

        // Keep Input disabled during game start messages
        FTimerHandle EnablePlayerTimer;
        FTimerDelegate EnablePlayerDelegate = FTimerDelegate::CreateUObject(
            PlayerController, &APlayerControllerBase::SetPlayerEnabledState, true);
        GetWorldTimerManager().SetTimer(EnablePlayerTimer, EnablePlayerDelegate, StartDelay, false);
    }
}

void AToonTankGameModeBase::OnGameOver(bool bPlayerWon)
{
    GameOver(bPlayerWon);
}
