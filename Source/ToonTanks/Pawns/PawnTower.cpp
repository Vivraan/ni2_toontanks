// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTower.h"

#include "Kismet/GameplayStatics.h"
#include "PawnTank.h"

// Called when the game starts or when spawned
void APawnTower::BeginPlay()
{
    Super::BeginPlay();

    GetWorldTimerManager().SetTimer(FireRateTimer, this, &APawnTower::CheckFireCondition, FireRate, true);

    PlayerPawn = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void APawnTower::OnDeath()
{
    Super::OnDeath();
    Destroy();
}

// Called every frame
void APawnTower::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (!PlayerPawn || !PlayerPawn->IsPlayerAlive() || !IsPlayerWithinRange())
    {
        return;
    }

    TurnTurret(PlayerPawn->GetActorLocation());
}

// Called to bind functionality to input
void APawnTower::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void APawnTower::CheckFireCondition()
{
    if (!PlayerPawn || !PlayerPawn->IsPlayerAlive())
    {
        return;
    }

    if (IsPlayerWithinRange())
    {
        Fire();
    }
}

bool APawnTower::IsPlayerWithinRange() const
{
    float SqrDistance = PlayerPawn ? FVector::DistSquared(PlayerPawn->GetActorLocation(), GetActorLocation()) : 0.f;
    return SqrDistance <= FMath::Square(DetectionRange);
}
