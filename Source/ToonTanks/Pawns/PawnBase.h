// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "PawnBase.generated.h"

UCLASS()
class TOONTANKS_API APawnBase : public APawn
{
    GENERATED_BODY()

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UCapsuleComponent* Capsule;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* BaseMesh;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* TurretMesh;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    USceneComponent* ProjectileSpawnPoint;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UHealthComponent* HealthComponent;


    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties|Projectile Type", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<class AProjectileBase> ProjectileClass;

    UPROPERTY(EditAnywhere, Category = "Properties|Effects")
    UParticleSystem* DeathParticles;

    UPROPERTY(EditAnywhere, Category = "Properties|Effects")
    USoundBase* DeathSound;

    UPROPERTY(EditAnywhere, Category = "Properties|Effects")
    TSubclassOf<UCameraShakeBase> DeathShake;

public:
    // Sets default values for this pawn's properties
    APawnBase();

    virtual void OnDeath();

protected:
    void TurnTurret(const FVector& TargetLocation);

    void Fire();
};
