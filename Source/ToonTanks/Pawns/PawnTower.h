// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"

#include "PawnTower.generated.h"

/**
 *
 */
UCLASS()
class TOONTANKS_API APawnTower : public APawnBase
{
    GENERATED_BODY()

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    float FireRate = 2.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
    float DetectionRange = 500.f;

    FTimerHandle FireRateTimer;
    class APawnTank* PlayerPawn;

    void CheckFireCondition();
    bool IsPlayerWithinRange() const;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    virtual void OnDeath() override;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
};
