// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"

#include "PawnTank.generated.h"

/**
 *
 */
UCLASS()
class TOONTANKS_API APawnTank : public APawnBase
{
    GENERATED_BODY()

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* SpringArm;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* Camera;

    /**
     * cm/s
     * Try to keep it at 5 times the Turn Speed.
     */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
    float MoveSpeed = 100.f;

    /**
     *  degrees/s
     * Try to keep it at 1/5th the Move Speed.
     */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
    float TurnSpeed = 100.f;

    FVector MoveDirection;
    FQuat TurnOrientation;

    APlayerController* PlayerController;
    bool bPlayerAlive = true;

    void CalculateMoveInput(float Throw);
    void CalculateTurnInput(float Throw);

    void Move();
    void Turn();

public:
    // Sets default values for this pawn's properties
    APawnTank();

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    virtual void OnDeath() override;

    bool IsPlayerAlive() const;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
};
