// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTank.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "ToonTanks/ToonTanks.h"

APawnTank::APawnTank()
{
    CDSO(SpringArm, USpringArmComponent)
    SpringArm->SetupAttachment(RootComponent);

    CDSO(Camera, UCameraComponent)
    Camera->SetupAttachment(SpringArm);
}

// Called when the game starts or when spawned
void APawnTank::BeginPlay()
{
    Super::BeginPlay();

    PlayerController = Cast<APlayerController>(GetController());
}

void APawnTank::OnDeath()
{
    Super::OnDeath();

    // TODO Hide player
    bPlayerAlive = false;
    SetActorHiddenInGame(true);
    SetActorTickEnabled(false);
}

bool APawnTank::IsPlayerAlive() const
{
    return bPlayerAlive;
}

// Called every frame
void APawnTank::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    Turn();
    Move();

    if (PlayerController)
    {
        FHitResult Hit;
        if (PlayerController->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
        {
            TurnTurret(Hit.ImpactPoint);
        }
    }
}

// Called to bind functionality to input
void APawnTank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &APawnTank::CalculateMoveInput);
    PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APawnTank::CalculateTurnInput);
    PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &APawnTank::Fire);
}

void APawnTank::CalculateMoveInput(float Throw)
{
    MoveDirection = FVector{ Throw * MoveSpeed * GetWorld()->DeltaTimeSeconds, 0.f, 0.f };
}

void APawnTank::CalculateTurnInput(float Throw)
{
    TurnOrientation = FQuat{ FVector::ZAxisVector, FMath::DegreesToRadians(Throw * TurnSpeed * GetWorld()->DeltaTimeSeconds) };
}

void APawnTank::Move()
{
    AddActorLocalOffset(MoveDirection, true);
}

void APawnTank::Turn()
{
    AddActorLocalRotation(TurnOrientation, true);
}
