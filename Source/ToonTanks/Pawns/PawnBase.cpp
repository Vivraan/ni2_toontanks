// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBase.h"

#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/Actors/ProjectileBase.h"
#include "ToonTanks/Components/HealthComponent.h"
#include "ToonTanks/ToonTanks.h"

// Sets default values
APawnBase::APawnBase()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    CDSO(Capsule, UCapsuleComponent)
    RootComponent = Capsule;

    CDSO(BaseMesh, UStaticMeshComponent)
    BaseMesh->SetupAttachment(RootComponent);

    CDSO(TurretMesh, UStaticMeshComponent)
    TurretMesh->SetupAttachment(BaseMesh);

    CDSO(ProjectileSpawnPoint, USceneComponent)
    ProjectileSpawnPoint->SetupAttachment(TurretMesh);

    CDSO(HealthComponent, UHealthComponent)
}

void APawnBase::TurnTurret(const FVector& TargetLocation)
{
    // Adjust TargetLocation to avoid wonky yaw rotations
    FVector TargetLocationAdjusted = FVector{ TargetLocation.X, TargetLocation.Y, TurretMesh->GetComponentLocation().Z };
    FVector StartLocation = TurretMesh->GetComponentLocation();

    TurretMesh->SetWorldRotation((TargetLocationAdjusted - StartLocation).Rotation());
}

void APawnBase::Fire()
{
    if (ProjectileClass)
    {
        AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(
            ProjectileClass,
            ProjectileSpawnPoint->GetComponentLocation(),
            ProjectileSpawnPoint->GetComponentRotation());

        TempProjectile->SetOwner(this);
    }
}

void APawnBase::OnDeath()
{
    // Play deathfx, sound, camera shake
    UGameplayStatics::SpawnEmitterAtLocation(this, DeathParticles, GetActorLocation());
    UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
    GetWorld()->GetFirstPlayerController()->ClientStartCameraShake(DeathShake);
}
